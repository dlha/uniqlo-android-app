package com.dlha.uniqlo.Model

class Category(val title : String, val image : String) {

    override fun toString() : String = title
}