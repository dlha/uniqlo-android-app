package com.dlha.uniqlo.Controller

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.dlha.uniqlo.Adapters.CategoryRecycleAdapter
import com.dlha.uniqlo.Services.DataService
import com.dlha.uniqlo.Utilities.EXTRA_CATEGORY
import com.dlha.uniqlo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var adapter : CategoryRecycleAdapter
    lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        adapter = CategoryRecycleAdapter(
            this,
            DataService.categories
        ) {
            val intent = Intent(this, ProductsActivity::class.java)
                .putExtra(EXTRA_CATEGORY, it.title)

            startActivity(intent)
        }

        binding.categoryListView.adapter = adapter

        val layoutManager = LinearLayoutManager(this)
        binding.categoryListView.layoutManager = layoutManager
        binding.categoryListView.setHasFixedSize(true)
    }
}