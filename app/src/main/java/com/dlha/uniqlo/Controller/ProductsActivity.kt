package com.dlha.uniqlo.Controller

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.dlha.uniqlo.Adapters.ProductsAdapter
import com.dlha.uniqlo.Services.DataService
import com.dlha.uniqlo.Utilities.EXTRA_CATEGORY
import com.dlha.uniqlo.databinding.ActivityProductsBinding

class ProductsActivity : AppCompatActivity() {
    lateinit var adapter : ProductsAdapter
    lateinit var binding : ActivityProductsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityProductsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val categoryType = intent.getStringExtra(EXTRA_CATEGORY)
        adapter = ProductsAdapter(this, DataService.getProducts(categoryType)) {
            Toast.makeText(this, "You clicked on ${it.title}", Toast.LENGTH_SHORT).show()
        }

        val layoutManager = GridLayoutManager(this, 2)
        binding.productsListView.adapter = adapter
        binding.productsListView.layoutManager = layoutManager

    }
}